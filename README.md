Docker-role
===========

Ansible роль, которая устанавливает docker

Описание действий
-----------------

1. Установка зависимостей
2. Добавление gpg-ключа
3. Добавление репозитория
4. Установка docker и docker compose plugin
5. Запуск и автозагрузка сервиса
6. Опционально. Добавление пользователей в группу docker
7. Опционально. Установка python docker модуля

Requirements
------------

Debian-подобный дистрибутив

Role Variables
--------------

Переменные хранятся в *vars/main.yml* 

    docker_users:
      - username

Dependencies
------------

Нет

Example Playbook
----------------

    - hosts: docker-servers
      become: true
      roles:
        - "ansible-docker-role"
